//
//  TabBarController.swift
//  TravelSport
//
//  Created by Mirko Calce on 22/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import Foundation
import UIKit

class TabBarController:UITabBarController
{

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let background = UIImage(named: "pic.jpg")
        let imageView = UIImageView(frame: view.bounds)
        
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        view.addSubview(imageView)
        self.view.sendSubviewToBack(imageView)
        
        let effect = UIImageView(frame: view.bounds)
        effect.contentMode = UIView.ContentMode.scaleAspectFill
        effect.clipsToBounds = true
        effect.center = view.center
        effect.alpha = 0.55
        effect.backgroundColor = UIColor.lightGray
        self.view.insertSubview(effect, at: 1)
        
        let blurEffect = UIBlurEffect(style: .dark) // here you can change blur style
        let blurView = UIVisualEffectView(effect: blurEffect)
        
        tabBar.isTranslucent = true
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage() // add this if you want remove tabBar separator
        tabBar.barTintColor = .clear
        tabBar.backgroundColor = .black // here is your tabBar color
        tabBar.layer.backgroundColor = UIColor.clear.cgColor
        
        blurView.frame = self.view.bounds
        blurView.translatesAutoresizingMaskIntoConstraints = false
        tabBar.insertSubview(blurView, at: 0)
        
        navigationController?.navigationBar.barStyle = .black
    }
    
}
