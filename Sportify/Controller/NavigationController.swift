//
//  NavigationBarController.swift
//  TravelSport
//
//  Created by Mirko Calce on 23/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
    
}
