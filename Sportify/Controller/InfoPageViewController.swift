//
//  InfoPageViewController.swift
//  TravelSport
//
//  Created by Ekaterina Musiyko on 21/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//


import UIKit
import dJsoN_lib

class InfoPageViewController: UIViewController {
    
    @IBOutlet weak var infoPageNavigation: UINavigationBar!
    @IBOutlet weak var textAbout: UITextView!
    @IBOutlet weak var infoPageTitle: UILabel!
    
    var nameCity: String?
    var nameSport: String?
    var infoAbout: String?
    var name: String = " "
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = .black
        if nameCity != nil
        {
            name = nameCity!
            infoPageTitle.text = name
            textAbout.text = getCityDescription(cityName: name)
        }
        else  if nameSport != nil
        {
            name = nameSport!
            infoPageTitle.text = name
            textAbout.text = getSportDescription(sportName: name)
        }
        else
        {
            infoPageTitle.text = "Something gone wrong"
            textAbout.text = "FAIL"
        }
    }
}
