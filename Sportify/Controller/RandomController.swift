//
//  RandomController.swift
//  TravelSport
//
//  Created by Luigi Ascione on 20/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import Foundation
import UIKit
import CoreMotion

class RandomController: UIViewController
{
    
    var motion = CMMotionManager()
    @IBOutlet weak var RechargeButton: UIButton!
    
    let shakeimage = UIImageView()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        view.backgroundColor = .black
        shakeimage.image = UIImage(named: "shake")
        shakeimage.frame = CGRect(x: 35, y: 300, width: 500, height: 280)
        view.addSubview(shakeimage)
    
        self.becomeFirstResponder()
    }
    
    override var canBecomeFirstResponder: Bool
    {
        get
        {
            return true
        }
    }

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?)
    {
        if motion == .motionShake
        {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            storyBoard.instantiateViewController(withIdentifier: "ResultRandomView")
            self.performSegue(withIdentifier: "ResultRandomView", sender: UIEvent())
        }
    }
}
