//
//  Caving.swift
//  TravelSport
//
//  Created by Luigi Ascione on 24/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import Foundation
import UIKit

class Caving: UIViewController {
    
    @IBOutlet weak var imageMap: UIImageView!
    @IBOutlet weak var imagePlace1: UIImageView!
    @IBOutlet weak var imagePlace3: UIImageView!
    @IBOutlet weak var imagePlace2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageMap.image = UIImage(named: "mappa")
        imagePlace1.image = UIImage(named: "notphoto")
        imagePlace2.image = UIImage(named: "notphoto")
        imagePlace3.image = UIImage(named: "notphoto")
        
    }
}
