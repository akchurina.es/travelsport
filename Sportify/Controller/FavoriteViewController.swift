//
//  FavoriteViewController.swift
//  TravelSport
//
//  Created by Михаил on 21.11.2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import UIKit
import dJsoN_lib

class FavoriteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var imageIsChanged = false

    @IBOutlet weak var avatarImageOutlet: UIImageView! {
        didSet {
            avatarImageOutlet.layer.cornerRadius = avatarImageOutlet.frame.size.height / 2
            avatarImageOutlet.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameLabelOutlet: UILabel!

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame: CGRect(x: 0,
                                                         y: 0,
                                                         width: tableView.frame.size.width,
                                                         height: 1))
        
        let storage = UserDefaults.standard
        nameLabelOutlet.text = storage.value(forKey: "name") as? String ?? "Insert your name"
        if let imageData = storage.value(forKey: "Avatar") as? Data {
            avatarImageOutlet.image = UIImage(data: imageData)
        } else {
            avatarImageOutlet.image = #imageLiteral(resourceName: "Photo")
        }
        
        // Do any additional setup after loading the view.
    }
    // MARK: - Setup TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if segmentedControl.selectedSegmentIndex == 0
        {
            return getSportFavourites().count
        } else
        {
            return getCityFavourites().count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        
        if segmentedControl.selectedSegmentIndex == 0
        {
            print(cell)
            let sports = getSportFavourites()
            showSportImage(sportName: sports[indexPath.row].name, view: cell.imageSportCell)
            cell.lableCell.text = sports[indexPath.row].name
            
        }
        else
        {
            let cities = getCityFavourites()
            showCityImage(cityName: cities[indexPath.row].name, view: cell.imageSportCell)
            cell.lableCell.text = cities[indexPath.row].name
            
        }
        return cell
    }
    
    // MARK:- Delete cell action
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete")
        {   _, _, complete in
            if self.segmentedControl.selectedSegmentIndex == 0
            {
                let sports = getSportFavourites()
                _ = toggleSportFavourite(selected: sports[indexPath.row])
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                saveSports()
            }
            else
            {
                let cities = getCityFavourites()
                _ = toggleCityFavourite(selected: cities[indexPath.row])
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                saveCities()
            }
            complete(true)
        }
        
        deleteAction.backgroundColor = .red
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // MARK:- Action for change avatar
    @IBAction func changeAvatarImage(_ sender: Any) {
        let cameraIcon = #imageLiteral(resourceName: "cameraIcon")
        let photoIcon = #imageLiteral(resourceName: "photoIcon")
        
        let actionSheet = UIAlertController(title: nil,
                                            message: nil,
                                            preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { _ in
            self.chooseImagePicker(source: .camera)
        }
        
        camera.setValue(cameraIcon, forKey: "image")
        camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        let photo = UIAlertAction(title: "Photo", style: .default) { _ in
            self.chooseImagePicker(source: .photoLibrary)
        }
        photo.setValue(photoIcon, forKey: "image")
        photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        actionSheet.addAction(camera)
        actionSheet.addAction(photo)
        actionSheet.addAction(cancel)

        present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK:- Info for change Name
    @IBAction func changeInfoName(_ sender: Any)
    {
        print("tab info")
        let actionSheet = UIAlertController(title: "Info",
                                            message: "Hold your finger to edit this.",
                                            preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Close", style: .cancel)
        actionSheet.addAction(cancel)
        present(actionSheet, animated: true, completion: nil)
    }
    
    // MARK:- Change City and Sport
    @IBAction func changeValue(_ sender: Any) {
        tableView.reloadData()
    }
    
    // MARK:- Change Name and Age
    @IBAction func changeNameAge(_ sender: Any)
    {
        let alertController = UIAlertController(title: "Insert your name", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = ""
        }
        let saveAction = UIAlertAction(title: "Save", style: .default, handler:
        {   alert -> Void in
            if(alertController.textFields![0].text == "" || alertController.textFields![0].text == nil)
            {
                alertController.textFields![0].text = "Insert your name"
            }
            self.nameLabelOutlet.text = alertController.textFields![0].text
            let storage = UserDefaults.standard
            storage.set(alertController.textFields![0].text, forKey: "name")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler:
        {
            (action : UIAlertAction!) -> Void in
        })

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK:- Work with PickerController
extension FavoriteViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func chooseImagePicker(source: UIImagePickerController.SourceType) {
    
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        avatarImageOutlet.image = info[.editedImage] as? UIImage
        avatarImageOutlet.contentMode = .scaleAspectFill
        avatarImageOutlet.clipsToBounds = true
        imageIsChanged = true
        dismiss(animated: true, completion: nil)
        let storage = UserDefaults.standard
        let dataImageAvatar = avatarImageOutlet.image?.pngData()
        storage.set(dataImageAvatar, forKey: "Avatar")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
