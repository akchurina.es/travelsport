//
//  CustomTableViewCell.swift
//  TravelSport
//
//  Created by Михаил on 22.11.2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var imageSportCell: UIImageView! {
        didSet {
            imageSportCell.layer.cornerRadius = 10
            imageSportCell.clipsToBounds = true
        }
    }
    
    @IBOutlet weak var lableCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
