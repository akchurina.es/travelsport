//
//  ResultRandomController.swift
//  TravelSport
//
//  Created by Luigi Ascione on 21/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import Foundation
import UIKit
import dJsoN_lib

class ResultRandomController: UIViewController
{
    
    @IBOutlet weak var ImageCity: UIImageView!
    @IBOutlet weak var ImageSport: UIImageView!
    
    
    @IBOutlet weak var StarCity: UIImageView!
    @IBOutlet weak var StarSport: UIImageView!
    
    @IBOutlet weak var LabelCity: UILabel!
    @IBOutlet weak var LabelSport: UILabel!
    
    var namecity: String = ""
    var namesport: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        StarSport.tintColor = .systemBlue
        StarCity.tintColor = .systemBlue
        view.backgroundColor = .black
        
        loadCities()
        loadSports()
        
        let sport = getRandomSport()
        let cit = getCityByName(cityName: sport.cityName)
        
        namesport = sport.name
        namecity = cit[0].name
        
        StarSport.isHighlighted = (sport.favourite == true ? true : false)
        StarCity.isHighlighted = (cit[0].favourite == true ? true : false)
        
        showCityImage(cityName: namecity, view: ImageCity)
        showSportImage(sportName: namesport, view: ImageSport)

        LabelCity.text = namecity
        LabelSport.text = namesport
        
        ImageCity.isUserInteractionEnabled = true
        ImageSport.isUserInteractionEnabled = true
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.openCity))
        ImageCity.addGestureRecognizer(tapGesture1)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.openSport))
        ImageSport.addGestureRecognizer(tapGesture2)
                
        StarCity.isUserInteractionEnabled = true
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.addCity))
        StarCity.addGestureRecognizer(tapGesture3)
        
        StarSport.isUserInteractionEnabled = true
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.addSport))
        StarSport.addGestureRecognizer(tapGesture4)
    }
    
    @objc func openCity()
    {
        self.performSegue(withIdentifier: "infoCountry", sender: UITapGestureRecognizer.self)
    }
    
    @objc func openSport()
    {
        self.performSegue(withIdentifier: "infoSport", sender: UITapGestureRecognizer.self)
    }
    
    @objc func addCity()
    {
        if StarCity.isHighlighted == false { StarCity.isHighlighted = true }
        else { StarCity.isHighlighted = false }
        if toggleCityFavouriteByName(cityName: namecity) == nil { print("Error!") }
        saveCities()
    }
    
    @objc func addSport()
    {
        if StarSport.isHighlighted == false { StarSport.isHighlighted = true }
        else { StarSport.isHighlighted = false }
        if toggleSportFavouriteByName(sportName: namesport, cityName: namecity) == nil { print("Error!") }
        saveSports()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "infoCountry"
        {
            let destinationVC = segue.destination as! InfoPageViewController
            destinationVC.nameCity = namecity
        }
        else if segue.identifier == "infoSport"
        {
            let destinationVC = segue.destination as! InfoPageViewController
            destinationVC.nameSport = namesport
        }
    }
}
    
