//
//  NearMeViewController.swift
//  TravelSport
//
//  Created by Mirko Calce on 22/11/2019.
//  Copyright © 2019 Ekaterina Musiyko. All rights reserved.
//

import Foundation
import UIKit
import dJsoN_lib


class NearMeViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var country: UILabel!
    
    @IBOutlet weak var imageMountainSkiing: UIImageView!
    @IBOutlet weak var imageCaving: UIImageView!
    @IBOutlet weak var imageRafting: UIImageView!
    @IBOutlet weak var imageNordicWalking: UIImageView!
    
    override func viewDidLoad()
    {
        view.backgroundColor = .black
        super.viewDidLoad()
        
        onRegister()
        
        loadCities()
        loadSports()
        
        country.text = "Switzerland"
        showCityImage(cityName: "Switzerland", view: image)
        
        image.contentMode = UIView.ContentMode.scaleAspectFill
        
        showSportImage(sportName: "Nordic Walking", view: imageNordicWalking)
        imageNordicWalking.contentMode = UIView.ContentMode.scaleAspectFill
        showSportImage(sportName: "Rafting", view: imageRafting)
        imageRafting.contentMode = UIView.ContentMode.scaleAspectFill
        showSportImage(sportName: "Caving", view: imageCaving)
        imageCaving.contentMode = UIView.ContentMode.scaleAspectFill
        showSportImage(sportName: "Mountain Skiing", view: imageMountainSkiing)
        imageMountainSkiing.contentMode = UIView.ContentMode.scaleAspectFill

        imageNordicWalking.isUserInteractionEnabled = true
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.open1))
        imageNordicWalking.addGestureRecognizer(tapGesture1)
        
        imageRafting.isUserInteractionEnabled = true
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.open2))
        imageRafting.addGestureRecognizer(tapGesture2)
        
        imageCaving.isUserInteractionEnabled = true
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.open3))
        imageCaving.addGestureRecognizer(tapGesture3)
        
        imageMountainSkiing.isUserInteractionEnabled = true
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.open4))
        imageMountainSkiing.addGestureRecognizer(tapGesture4)
        
    }

    @objc func open1(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "NordicWalking")
        self.present(secondViewController, animated:true, completion:nil)

    }
    
    @objc func open2() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "Rafting")
        self.present(secondViewController, animated:true, completion:nil)
    }
    
    @objc func open3() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "Caving")
        self.present(secondViewController, animated:true, completion:nil)

    }
    
    @objc func open4() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "MountainSkiing")
        self.present(secondViewController, animated:true, completion:nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}
